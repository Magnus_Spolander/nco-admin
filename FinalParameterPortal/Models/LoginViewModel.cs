﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace FinalParameterPortal.Models
{    
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Enter username")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Enter password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
                           
        public bool isValid(string userName, string password)
        {
            if (userName == "magnus" && password == "1")
            {
                return true;
            }
            else
                return false;
           
        }
    }    
}