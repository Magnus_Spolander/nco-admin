﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Models
{
    public class LogFile
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Country { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }

    public class LogFiles
    {
        public List<LogFile> files { get; set; }
    }
}