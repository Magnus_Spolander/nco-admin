﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Models
{
    public class DLLUser
    {

        [Required(ErrorMessage = "Enter your Final username")]
        [Display(Name = "DLL Username")]
        public string DllUserName { get; set; }

        public string DLLEmail { get; set; }
    }
}