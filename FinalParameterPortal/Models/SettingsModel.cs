﻿using FinalParameterPortal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Models
{
    public class SettingsModel
    {
        public ServiceManager Manager { get; set; }
        public List<ServiceModel> Services { get; set; }

        public SettingsModel()
        {
            this.Manager = new ServiceManager();
            this.Services = Manager.GetAll();
        }
    }
}