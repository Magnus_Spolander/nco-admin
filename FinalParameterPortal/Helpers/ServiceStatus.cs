﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Helpers
{
    public enum ServiceStatus
    {
        Stopped = 0,
        Running = 1
    }
}