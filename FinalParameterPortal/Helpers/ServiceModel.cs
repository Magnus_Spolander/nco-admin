﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Helpers
{
    public class ServiceModel
    {
        public string DisplayName { get; set; }
        public string SystemName { get; set; }
        public ServiceStatus Status { get; set; }        
        public DateTime LastChanged { get; set; }

        public ServiceModel()
        {
            this.DisplayName = string.Empty;
            this.SystemName = string.Empty;
            this.Status = ServiceStatus.Stopped;
            this.LastChanged = DateTime.MinValue;
        }
    }
}