﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Helpers
{
    public class ServiceManager : IDisposable
    {
        public List<ServiceModel> Services { get; set; }

        private string _settingsFile { get; set; }

        public ServiceManager()
        {
            Services = new List<ServiceModel>();
            _settingsFile = string.Concat(ConfigurationManager.AppSettings["SettingsFileLocation"], ConfigurationManager.AppSettings["SettingsFileName"]);
        }

        public List<ServiceModel> GetAll()
        {
            try
            {
                using (StreamReader r = new StreamReader(_settingsFile))
                {
                    string json = r.ReadToEnd();
                    List<ServiceModel> _services = JsonConvert.DeserializeObject<List<ServiceModel>>(json);

                    return _services;
                }
            }
            catch (Exception e)
            {
                // todo: log error
                throw e;
            }
        }

        public ServiceModel GetServiceInformationByName(string serviceName)
        {
            try
            {
                if (string.IsNullOrEmpty(serviceName))
                {
                    throw new Exception("When asking for Service Information by name, the serviceName-parameter was empty");
                }

                using (StreamReader r = new StreamReader(_settingsFile))
                {
                    string json = r.ReadToEnd();
                    List<ServiceModel> _services = JsonConvert.DeserializeObject<List<ServiceModel>>(json);

                    foreach (ServiceModel s in _services)
                    {
                        if (s.SystemName == serviceName)
                        {
                            return s;
                        }
                    }
                    throw new Exception(string.Concat("Found no information about a service called '", serviceName, "'."));
                }
            }
            catch (Exception e)
            {
                // todo: log error
                throw e;
            }
        }
        /// <summary>
        /// Returns a boolean value indicatig whether or not a service is running.
        /// </summary>
        /// <param name="SystemName">This value is the 'SystemName' value from the Settings-file.</param>
        /// <returns></returns>
        public bool IsServiceRunning(string SystemName)
        {
            try
            {
                ServiceModel s = this.GetServiceInformationByName(SystemName);
                if (s.Status == ServiceStatus.Running)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Save()
        {
            string json = string.Empty;

            try
            {
                foreach (ServiceModel s in this.Services)
                {
                    s.LastChanged = DateTime.Now; // Update the timestamp for last changed
                }

                json = JsonConvert.SerializeObject(this.Services);

                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(_settingsFile);
                file.WriteLine(json);

                file.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public void Dispose()
        {
            // throw new NotImplementedException();
        }
    }
}