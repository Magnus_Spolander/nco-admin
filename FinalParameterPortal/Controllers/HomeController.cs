﻿using FinalParameterPortal.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalParameterPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<string> availableServices = new List<string>();
            availableServices.Add("CRA");
            availableServices.Add("CRA_FI");
            availableServices.Add("FINAL");
            availableServices.Add("ICS");
            availableServices.Add("Credit_Hub");
            availableServices.Add("NCO");
            availableServices.Add("RKI");

            ServiceManager mgr = new ServiceManager();
            Dictionary<string, bool> servicesDictionary = new Dictionary<string, bool>();

            foreach (string sName in availableServices)
            {
                bool isRunning = mgr.IsServiceRunning(sName);
                servicesDictionary.Add(sName, isRunning);
            }

            return View(servicesDictionary);
        }
    }
}
