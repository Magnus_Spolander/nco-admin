﻿using FinalParameterPortal.Helpers;
using FinalParameterPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace FinalParameterPortal.Controllers
{
    public class SettingsController : Controller
    {
        public ActionResult Overview()
        {
            SettingsModel settings = new SettingsModel();
            settings.Manager.GetAll();
            return View(settings);
        }

        public ActionResult Save(List<ServiceModel> _services)
        {
            if (_services != null && _services.Count() > 0)
            {
                try
                {
                    ServiceManager manager = new ServiceManager();
                    manager.Services = _services;
                    manager.Save();
                }
                catch (Exception e)
                {                    
                    TempData["ErrorMessages"] = e.Message;
                    return PartialView("_ServicesSettings", _services);
                }
            }

            //return PartialView("_ServicesSettings", _services);
            return RedirectToAction("Index", "Home");
        }
    }
}
