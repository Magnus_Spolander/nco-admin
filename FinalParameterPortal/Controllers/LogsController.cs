﻿using FinalParameterPortal.Models;
using FinalParameterPortal.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace FinalParameterPortal.Controllers
{
    public class LogsController : Controller
    {
        //
        // GET: /Logs/

        public ActionResult DK()
        {
            var files = GetLogs(CountryCodes.Denmark);
            return View("Logs", files);               
        }

        public ActionResult FI()
        {           
            var files = GetLogs(CountryCodes.Finland);
            return View("Logs", files);
        }

        public ActionResult NO()
        {
            var files = GetLogs(CountryCodes.Norway);
            return View("Logs", files);
        }

        public ActionResult SE()
        {
            var files = GetLogs(CountryCodes.Sweden);
            return View("Logs", files);
        }
        
        public ActionResult Logs()
        {
           
            var files = GetLogs("");
            return View(files);
        }

        public FileResult Download(string id, string fileName)
        {
            int fid = Convert.ToInt32(id);

            string countryCode = GetCountryCode(fileName);
            var files = GetLogs(countryCode);
            string filename = (from f in files
                               where f.FileId == fid
                               select f.FilePath).First();
            string contentType = "txt";
            //Parameters to file are
            //1. The File Path on the File Server
            //2. The content type MIME type
            //3. The parameter for the file save by the browser
            // set fileName for old logs (which have date as ending)
            string logName = (from f in files
                              where f.FileId == fid
                              select f.FileName).First();
            string logDisplayName = logName.EndsWith("txt") ? logName : string.Format("{0}.txt", logName);

            string logFileLocation = ConfigurationManager.AppSettings["LogFileLocation"] + "\\" + logName;

            Stream stream = System.IO.File.Open(logFileLocation, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //File.OpenRead(seclogPath1);
            //StreamReader streamReader = new StreamReader(stream);
            //string str = streamReader.ReadToEnd();

            var ret = File(stream, contentType, logDisplayName);
            //streamReader.Close();
            //stream.Close();

            return ret;

        }

        private string GetCountryCode(string fileName)
        {
            if (fileName.Contains(CountryCodes.Finland))
            {
                return CountryCodes.Finland;
            }
            else if (fileName.Contains(CountryCodes.Norway))
            {
                return CountryCodes.Norway;
            }
            else if (fileName.Contains(CountryCodes.Sweden))
            {
                return CountryCodes.Sweden;
            }
            else
                return CountryCodes.Denmark;
        }

        public List<LogFile> GetLogs(string countryCode)
        {

            string logFileLocation, logFileName;
            try
            {
                logFileLocation = ConfigurationManager.AppSettings["LogFileLocation"];
                logFileName = ConfigurationManager.AppSettings["LogFileName"];
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to get log file values from config file!", ex);
            }

            List<LogFile> logs = new List<LogFile>();
            DirectoryInfo dirInfo = new DirectoryInfo(logFileLocation);

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                if (countryCode == string.Empty)
                {
                    if (item.Name.StartsWith(logFileName) && !item.Name.Contains("_"))
                    {
                        logs.Add(
                            new LogFile()
                            {
                                FileId = i + 1,
                                FileName = item.Name,
                                FilePath = dirInfo.FullName + @"\" + item.Name,
                                Created = item.CreationTime,
                                Updated = item.LastWriteTime,
                                Country = "DK"
                            });
                        i = i + 1;
                    }
                }
                else
                {
                    if (item.Name.StartsWith(logFileName) && item.Name.Contains(countryCode))
                    {
                        logs.Add(
                            new LogFile()
                            {
                                FileId = i + 1,
                                FileName = item.Name,
                                FilePath = dirInfo.FullName + @"\" + item.Name,
                                Created = item.CreationTime,
                                Updated = item.LastWriteTime,
                                Country = countryCode
                            });
                        i = i + 1;
                    }
                }
                
                
                
                
                
            }



            logs.Sort((file1, file2) => file2.Updated.CompareTo(file1.Updated));

            return logs;
        }        
    }
}
