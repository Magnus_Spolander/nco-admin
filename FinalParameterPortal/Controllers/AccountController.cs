﻿using FinalParameterPortal.Domain;
using FinalParameterPortal.Helper;
using FinalParameterPortal.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;



namespace FinalParameterPortal.Controllers
{
    public class AccountController : Controller
    {
        string cookieName = "_rememberMe_FPP";
        string userName = null;

        [HttpGet]
        public ActionResult Login()
        {

            if (Request.Cookies[cookieName] != null)
            {
                userName = Encryption.Unprotect(Request.Cookies[cookieName].Value);
                ViewBag.RememberMeUserName = userName;
            }
                           
            if (User.Identity.IsAuthenticated)
            {
                return View("Index", "Home");
            }
            else
                return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Models.LoginViewModel loginUser, string check = null)
        {
            if (ModelState.IsValid)
            {
                //if (loginUser.isValid(loginUser.UserName, loginUser.Password))
                if (FormsAuthentication.Authenticate(loginUser.UserName, loginUser.Password) == true)                
                {
                    if (check != null) // Remember Me checked - create 30 day cookie
                    {
                        userName = Encryption.Protect(loginUser.UserName);
                        HttpCookie cookie = Cookies.createCookie(cookieName, userName);
                        Response.Cookies.Add(cookie);
                    }
                    else if (Request.Cookies[cookieName] != null) // Cookie exist, but Remember me is not checked - delete cookie
                    {
                        HttpCookie cookie = Cookies.deleteCookie(cookieName);
                        Response.Cookies.Add(cookie);
                    }

                    FormsAuthentication.SetAuthCookie(loginUser.UserName, loginUser.RememberMe);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("loginError", "Login data is incorrect");
                }
            }
            return View(loginUser);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();            
            return View();
        }
        
        public ActionResult SearchFinalUser()
        {
            return View();
        }
    }
}
