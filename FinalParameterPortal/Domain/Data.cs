﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Domain
{
    public class Data
    {
        public IEnumerable<Navbar> navbarItems()
        {
            var menu = new List<Navbar>();
            //menu.Add(new Navbar { Id = 1, nameOption = "Home", controller = "Home", action = "Index", imageClass = "active", estatus = true, isParent = false, parentId = 0 });
            // Settings //
            menu.Add(new Navbar { Id = 2, nameOption = "Settings", controller = "Settings", action = "Overview", estatus = true, isParent = false, parentId = 0 });
            // Logs //
            menu.Add(new Navbar { Id = 3, nameOption = "Logs", controller = "Home", action = "Index", estatus = true, isParent = true, parentId = 0 });
            menu.Add(new Navbar { Id = 4, nameOption = "Denmark", controller = "Logs", action = "DK", estatus = true, isParent = false, parentId = 3 });
            menu.Add(new Navbar { Id = 5, nameOption = "Finland", controller = "Logs", action = "FI", estatus = true, isParent = false, parentId = 3 });
            menu.Add(new Navbar { Id = 6, nameOption = "Norway", controller = "Logs", action = "NO", estatus = true, isParent = false, parentId = 3 });
            menu.Add(new Navbar { Id = 7, nameOption = "Sweden", controller = "Logs", action = "SE", estatus = true, isParent = false, parentId = 3 });           
            
            return menu.ToList();
        }

        public IEnumerable<User> getUsers()
        {
            var users = new List<User>();
            users.Add(new User { Id = 1, user = "admin", password = "12345", estatus = true, RememberMe = true });
            users.Add(new User { Id = 2, user = "lvasquez", password = "lvasquez", estatus = true, RememberMe = false });
            users.Add(new User { Id = 3, user = "invite", password = "12345", estatus = false, RememberMe = false });

            return users.ToList();
        }
    }
}