﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;

namespace FinalParameterPortal.Helper
{
    public class Encryption
    {
        static string purpose = "Username: FPP_user";

        public static string Protect(string textToProtect)
        {          
            try
            {
                byte[] stream = Encoding.UTF8.GetBytes(textToProtect);
                byte[] encodedValue = MachineKey.Protect(stream, purpose);
                return HttpServerUtility.UrlTokenEncode(encodedValue);
            }
            catch (Exception)
            {
                return string.Empty;
            }                        
        }

        public static string Unprotect(string textToUnprotect)
        {            
            try
            {
                byte[] stream = HttpServerUtility.UrlTokenDecode(textToUnprotect);
                byte[] decodedValue = MachineKey.Unprotect(stream, purpose);
                return Encoding.UTF8.GetString(decodedValue);
            }
            catch (Exception)
            {
                return string.Empty;
            }            
        }        
    }
}