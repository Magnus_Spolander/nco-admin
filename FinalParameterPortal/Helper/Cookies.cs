﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalParameterPortal.Helper
{
    public class Cookies
    {
   
        public static HttpCookie createCookie(string cookieName, string cookieValue)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.Now.AddDays(30d);
            cookie.HttpOnly = true;
            cookie.Value = cookieValue;
            return cookie;
        }

        public static HttpCookie deleteCookie(string cookieName)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.Now.AddDays(-1d);
            return cookie;
        }
    }
}